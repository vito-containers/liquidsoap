## Liquidsoap Container Registry

This GitLab repository serves as a container registry for Liquidsoap images.

### Pulling Images

Images can be pulled from this registry using:

`docker pull registry.gitlab.com/vito-containers/liquidsoap:$TAGNAME`

Replace `$TAGNAME` with one of the available tags:

### Tags

#### From official packages

- [`latest`][2.2-dockerfile],
  [`2.2`][2.2-dockerfile],
  [`2.2.3`][2.2-dockerfile]
- [`2.2-minimal`][2.2-minimal-dockerfile]
  [`2.2.3-minimal`][2.2-minimal-dockerfile]
- [`2.1`][2.1-dockerfile]
  [`2.1.4`][2.1-dockerfile]
- [`2.0`][2.0-dockerfile]
  [`2.0.7`][2.0-dockerfile]
- [`1.4`][1.4-dockerfile]
  [`1.4.4`][1.4-dockerfile]

#### Slim build from opam

These images are built using custom packages from opam.
Images can be pulled from registry using:

`docker pull registry.gitlab.com/vito-containers/liquidsoap/alpine-opam:$TAGNAME`

- [`latest`][2.2-alpine-opam-dockerfile]
  [`2.2`][2.2-alpine-opam-dockerfile]
  [`2.2.3`][2.2-alpine-opam-dockerfile]

### Build Projects

The following projects can build and push images to this registry:

- [ubuntu] - Liquidsoap builds from official packages
  based on ubuntu.
- [alpine-opam] - Liquidsoap builds from opam based on
  alpine.

[1.4-dockerfile]: https://gitlab.com/vito-containers/liquidsoap-builds/ubuntu/-/blob/main/containers/1.4/Dockerfile
[2.0-dockerfile]: https://gitlab.com/vito-containers/liquidsoap-builds/ubuntu/-/blob/main/containers/2.0/Dockerfile
[2.1-dockerfile]: https://gitlab.com/vito-containers/liquidsoap-builds/ubuntu/-/blob/main/containers/2.1/Dockerfile
[2.2-dockerfile]: https://gitlab.com/vito-containers/liquidsoap-builds/ubuntu/-/blob/main/containers/2.2/Dockerfile
[2.2-minimal-dockerfile]: https://gitlab.com/vito-containers/liquidsoap-builds/ubuntu/-/blob/main/containers/2.2-minimal/Dockerfile
[2.2-alpine-opam-dockerfile]: https://gitlab.com/vito-containers/liquidsoap-builds/alpine-opam/-/blob/main/containers/2.2/Dockerfile
[ubuntu]: https://gitlab.com/vito-containers/liquidsoap-builds/ubuntu/
[alpine-opam]: https://gitlab.com/vito-containers/liquidsoap-builds/alpine-opam/
